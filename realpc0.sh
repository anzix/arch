#!/bin/bash
# Позаимствован из 
# https://raw.githubusercontent.com/creio/dots/master/.bin/creio.sh

# Создан в первую очередь для Dual-Boot рядом с W10 (Пока что)
# Без Dual-Boot необходимо всего-лишь править цифры у переменных \
# DISK_EFI=... \
# DISK_MNT=... \
# И цифры связанные с разметкой "gdisk $DISK"

DISK=/dev/sda
DISK_EFI=/dev/sda5
DISK_MNT=/dev/sda6

# Базовые пакеты в /mnt
PKGS=(
  base base-devel reflector pacman-contrib openssh
#  linux linux-headers
  linux-zen linux-zen-headers
#  linux-lts linux-lts-headers
  linux-firmware
  zsh git wget neovim
  ntfs-3g exfat-utils dosfstools # Поддержка NTFS, exFAT, vFAT
#  os-prober mtools
  grub efibootmgr
  dhcpcd netctl
  intel-ucode
  xdg-user-dirs
  terminus-font # Шрифты разных размеров с кириллицей для tty
  ccache
  zram-generator
)

loadkeys ru
setfont cyr-sun16
timedatectl set-ntp true
sed -i "s/#\(en_US\.UTF-8\)/\1/; s/#\(ru_RU\.UTF-8\)/\1/" /etc/locale.gen
locale-gen
export LANG=ru_RU.UTF-8

clear
read -p "Имя хоста (hostname): " HOST_NAME

read -p "Имя пользователя (Может быть только в нижнем регистре и без знаков): " USER_NAME

read -sp "Пароль $USER_NAME: " PASSWORD
echo
read -sp "Подтвердите пароль: " C_PASSWORD
if [[ "$PASSWORD" != "$C_PASSWORD" ]]; then
  echo "Error: incorrect password"; exit 1
fi
echo

read -p "Sudo с запросом пароля? [Y/n]: " sudo_priv

read -p "Тип смены раскладки клавиатуры в tty
1 - Alt+Shift, 2 - Caps Lock: " xkb_layout

read -p "Файловая система
1 - ext4, 2 - btrfs: " FS


#sgdisk --zap-all $DISK  # Delete (destroy) the GPT and MBR data structures and then exit.
#wipefs --all --force $DISK # Erase all available signatures.
printf "\
n\n5\n\n+512M\nef00\nn\
n\n6\n\n\n\nw\ny\n"\
| gdisk $DISK

# Boot раздел n\n5\n\n+150M\nef00\nn\

# Файловая система
if [[ $FS == 1 ]]; then
  echo -e "\e[1;32mФайловая система Ext4\n\e[0m"
  yes | mkfs.fat -F32 -n EFI $DISK_EFI
  yes | mkfs.ext4 -L Root $DISK_MNT
  # yes | mkfs.ext4 $H_DISK -L home
  mount $DISK_MNT /mnt
  mkdir -p /mnt/boot/efi
  mount $DISK_EFI /mnt/boot/efi
  # mkdir /mnt/home
  # mount $H_DISK /mnt/home

  # При обнаружении приплюсовывается в список для pacstrap
  PKGS+=(e2fsprogs)
elif [[ $FS == 2 ]]; then
  echo -e "\e[1;32mФайловая система Btrfs\n\e[0m"
  yes | mkfs.fat -F32 -n EFI $DISK_EFI
  mkfs.btrfs -L Root -f $DISK_MNT
  mount -v $DISK_MNT /mnt

  btrfs su cr /mnt/@
  btrfs su cr /mnt/@home
  btrfs su cr /mnt/@snapshots
  btrfs su cr /mnt/@var_log
  btrfs su cr /mnt/@libvirt
  umount -v /mnt

  # BTRFS сам обнаруживает SSD при монтировании
  mount -v -o noatime,compress=zstd:2,space_cache=v2,subvol=@ $DISK_MNT /mnt
  mkdir -pv /mnt/{boot/efi,home,.snapshots,var/log,var/lib/libvirt}
  mount -v -o noatime,compress=zstd:2,space_cache=v2,subvol=@home $DISK_MNT /mnt/home
  mount -v -o noatime,compress=zstd:2,space_cache=v2,subvol=@snapshots $DISK_MNT /mnt/.snapshots
  mount -v -o noatime,compress=zstd:2,space_cache=v2,subvol=@var_log $DISK_MNT /mnt/var/log
  mount -v -o noatime,nodatacow,compress=zstd:2,space_cache=v2,subvol=@libvirt $DISK_MNT /mnt/var/lib/libvirt
  mount -v $DISK_EFI /mnt/boot/efi

  # При обнаружении приплюсовывается в список для pacstrap
  PKGS+=(btrfs-progs)
else
  echo "FS type"; exit 1
fi



echo -e "\e[1;32mПравка конфига pacman\n\e[0m"
sed -i "/#Color/a ILoveCandy" /etc/pacman.conf  # Делаем pacman красивее
sed -i "s/#Color/Color/g" /etc/pacman.conf  # Добавляем цвета в pacman
sed -i "s/#ParallelDownloads = 5/ParallelDownloads = 10/g" /etc/pacman.conf  # Увеличение паралельных загрузок с 5 на 10
sed -i "s/#VerbosePkgLists/VerbosePkgLists/g" /etc/pacman.conf # Более удобный просмотр лист пакетов
sed -i "/\[multilib\]/,/Include/"'s/^#//' /etc/pacman.conf # Раскоментирование строчки multilib для запуска 32bit приложений


## https://ipapi.co/timezone | http://ip-api.com/line?fields=timezone | https://ipwhois.app/line/?objects=timezone
time_zone=$(curl -s https://ipinfo.io/timezone)
timedatectl set-timezone $time_zone

echo -e "\e[1;32mОптимизация зеркал с помощью Reflector\n\e[0m"
reflector --verbose -c ru,by -p http,https -l 12 --sort rate --save /etc/pacman.d/mirrorlist


echo -e "\e[1;32mУстановка базовых пакетов в /mnt\n\e[0m"
pacstrap /mnt "${PKGS[@]}"


echo -e "\e[1;32mГенерирую fstab\n\e[0m"
genfstab -U /mnt >> /mnt/etc/fstab
# Make /tmp a ramdisk
echo "
tmpfs 	/tmp	tmpfs		rw,nodev,nosuid,size=8G	 0 0" >> /mnt/etc/fstab


# Обнаружение виртуалки
hypervisor=$(systemd-detect-virt)

echo -e "\e[1;32mChroot'имся\n\e[0m"
arch-chroot /mnt /bin/bash << EOF

# Руссифицируемся
sed -i "s/#\(en_US\.UTF-8\)/\1/; s/#\(ru_RU\.UTF-8\)/\1/" /etc/locale.gen
locale-gen
echo "LANG=ru_RU.UTF-8" > /etc/locale.conf
echo "LC_COLLATE=C" >> /etc/locale.conf

# Смена раскладки клавиатуры в tty
if [[ $xkb_layout == 1 ]]; then
  echo "KEYMAP=ruwin_alt_sh-UTF-8" > /etc/vconsole.conf
elif [[ $xkb_layout == 2 ]]; then
  echo "KEYMAP=ruwin_cplk-UTF-8" > /etc/vconsole.conf
fi
echo "FONT=ter-v22b" >> /etc/vconsole.conf

# Часовой пояс
ln -sf /usr/share/zoneinfo/$time_zone /etc/localtime

# Синхронизировать часы материнской платы
timedatectl set-ntp true
hwclock --systohc --utc 

# Имя хоста 
echo $HOST_NAME > /etc/hostname

echo "127.0.0.1 localhost" >> /etc/hosts
echo "::1       localhost" >> /etc/hosts
echo "127.0.1.1 $HOST_NAME.localdomain $HOST_NAME
" >> /etc/hosts

# Пароль root пользователя
echo root:$PASSWORD | chpasswd

# Добавления юзера и присваивание групп к юзеру
useradd -m -g users -G wheel,audio,video,input,optical,games -s /bin/zsh $USER_NAME
echo $USER_NAME:$PASSWORD | chpasswd

# Создание ключей Pacman
pacman-key --init
pacman-key --populate archlinux

echo -e "\e[1;32mПравка конфига pacman\n\e[0m"
sed -i "/#Color/a ILoveCandy" /etc/pacman.conf  # Делаем pacman красивее
sed -i "s/#Color/Color/g" /etc/pacman.conf  # Добавляем цвета в pacman
sed -i "s/#ParallelDownloads = 5/ParallelDownloads = 8/g" /etc/pacman.conf  # Увеличение паралельных загрузок с 5 на 8
sed -i "s/#VerbosePkgLists/VerbosePkgLists/g" /etc/pacman.conf # Более удобный просмотр лист пакетов
sed -i "/\[multilib\]/,/Include/"'s/^#//' /etc/pacman.conf # Раскоментирование строчки multilib для запуска 32bit приложений

# Обнаружение виртуалки
case $hypervisor in
  kvm )     echo ">KVM обнаружен."
            echo ">Устанавливаю гостевые инструменты."
            pacman -S qemu-guest-agent spice-vdagent --noconfirm --needed
            #echo ">Включение определенных служб для гостевых инструментов."
            #systemctl enable qemu-guest-agent
            ;;
  oracle )  echo ">VirtualBox обнружен."
            echo ">Устанавливаю гостевые инструменты."
            pacman -S virtualbox-guest-utils xf86-video-vmware --noconfirm --needed
            echo ">Включение определенных служб для гостевых инструментов."
            systemctl enable vboxservice
            # Shared Folder
            usermod -a -G vboxsf $USER_NAME
            ;;
  * ) ;;
esac

echo -e "\e[1;32mПравка mkinitcpio.conf\n\e[0m"
if [[ $FS == 2 ]]; then
  sed -i 's/^MODULES.*/MODULES=(btrfs amdgpu)/' /etc/mkinitcpio.conf
  # Add the btrfs binary in order to do maintenence on system without mounting it
  sed -i 's/^BINARIES=.*$/BINARIES=(btrfs)/' /etc/mkinitcpio.conf
  sed -i "s/^HOOKS.*/HOOKS=(base consolefont udev autodetect modconf block filesystems keyboard keymap)/g" /etc/mkinitcpio.conf
else
  sed -i 's/^MODULES.*/MODULES=(amdgpu)/' /etc/mkinitcpio.conf
  sed -i "s/^HOOKS.*/HOOKS=(base consolefont udev autodetect modconf block filesystems keyboard keymap fsck)/g" /etc/mkinitcpio.conf
fi
mkinitcpio -P
              
# Правка конфига reflector
sed -i "s/^--protocol.*/--protocol http,https/" /etc/xdg/reflector/reflector.conf
sed -i "s/# --country.*/--country ru,by/" /etc/xdg/reflector/reflector.conf
sed -i "s/^--latest.*/--latest 12/" /etc/xdg/reflector/reflector.conf
sed -i "s/^--sort.*/--sort rate/" /etc/xdg/reflector/reflector.conf

# Создаю Reflector hook
mkdir /etc/pacman.d/hooks
echo -e "[Trigger]
Operation = Upgrade
Type = Package
Target = pacman-mirrorlist

[Action]
Description = Updating pacman-mirrorlist with reflector and removing pacnew...
When = PostTransaction
Depends = reflector
Exec = /bin/sh -c 'systemctl start reflector.service; if [ -f /etc/pacman.d/mirrorlist.pacnew ]; then rm /etc/pacman.d/mirrorlist.pacnew; fi'" | tee -a /etc/pacman.d/hooks/mirrorupgrade.hook > /dev/null


# Pacman cache clean hook
echo -e "[Trigger]
Type = Package
Operation = Upgrade
Operation = Install
Operation = Remove
Target = *

[Action]
Description = Удаление устаревших кэшированных пакетов (с сохранением двух последних)...
When = PostTransaction
Exec = /usr/bin/paccache -rk2" | tee -a /etc/pacman.d/hooks/clean_package_cache.hook > /dev/null


# GRUB upgrade hooks
echo -e '[Trigger]
Type = Package
Operation = Upgrade
Target = grub
[Action]
Description = Upgrading GRUB...
When = PostTransaction
Exec = /usr/bin/sh -c "grub-install --efi-directory=/boot/efi; grub-mkconfig -o /boot/grub/grub.cfg"' | tee -a /etc/pacman.d/hooks/92-grub-upgrade.hook > /dev/null


# Исправляет dhcpcd вызываемый медленный старт
mkdir /etc/systemd/system/dhcpcd@.service.d/
echo -e "[Service]
ExecStart=
ExecStart=/usr/bin/dhcpcd -b -q %I" | tee -a /etc/systemd/system/dhcpcd@.service.d/no-wait.conf > /dev/null

# Ускоряет dhcp путём отключения Arp зондирования
sed -i '$ a\noarp' /etc/dhcpcd.conf

# Zram
echo -e "[zram0]
zram-size = min(min(ram, 4096) + max(ram - 4096, 0) / 2, 32 * 1024)
compression-algorithm = zstd" | tee -a /etc/systemd/zram-generator.conf > /dev/null

# Лучшие настройки подкачки для 16gb ОЗУ
echo -e "vm.swappiness=10
vm.vfs_cache_pressure=50" | tee -a /etc/sysctl.d/99-sysctl.conf > /dev/null


# Привелегии sudo
if [[ -z $sudo_priv || $sudo_priv == y || $sudo_priv == Y ]]; then
  echo -e "\e[1;32m>Привилегии sudo с запросом пароля\e[0m"
  sed -i 's/^# %wheel ALL=(ALL:ALL) ALL\.*/%wheel ALL=(ALL:ALL) ALL/' /etc/sudoers
elif [[ $sudo_priv == n || $sudo_priv == N ]]; then
  echo -e "\e[1;32m>Привилегии sudo без запроса пароля\e[0m"
  sed -i 's/^# %wheel ALL=(ALL:ALL) NOPASSWD: ALL\.*/%wheel ALL=(ALL:ALL) NOPASSWD: ALL/' /etc/sudoers
fi


echo -e "\e[1;32mУстановка Bootloader'а\n\e[0m"

# Добавления моих опций ядра grub
sudo sed -i 's/^GRUB_CMDLINE_LINUX_DEFAULT=.*/GRUB_CMDLINE_LINUX_DEFAULT="loglevel=3 mitigations=off pcie_aspm=off intel_iommu=on iommu=pt audit=0 nowatchdog amdgpu.ppfeaturemask=0xffffffff cpufreq.default_governor=performance intel_pstate=passive zswap.enabled=0"/g' /etc/default/grub

#sed -i -e 's/GRUB_GFXMODE=auto/GRUB_GFXMODE="1920x1080x32"/g' /etc/default/grub
#sed -i -e 's/#GRUB_DISABLE_OS_PROBER/GRUB_DISABLE_OS_PROBER/' /etc/default/grub # Обнаруживать другие ОС и добавлять их в grub (нужен пакет os-prober)
grub-install --efi-directory=/boot/efi
grub-mkconfig -o /boot/grub/grub.cfg


echo -e "\e[1;32mВрубаю сервисы\n\e[0m"
systemctl enable dhcpcd
systemctl enable sshd
systemctl enable fstrim.timer
# systemctl enable systemd-zram-setup@zram0.service (сам запускается, вроде)
systemctl enable systemd-oomd.service

EOF


if read -re -p "arch-chroot /mnt? [y/N]: " ans && [[ $ans == 'y' || $ans == 'Y' ]]; then
  arch-chroot /mnt
else
  umount -a # (-a) - безопасно размонтировать всё
fi

echo -e "\e[1;32mУстановка завершена! Выполните reboot чтобы перейти к следующему шагу\e[0m"

